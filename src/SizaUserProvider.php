<?php

namespace Siza\Auth;

use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Auth\UserProvider;
use Siza\Database\App\Models\Spsm\Employee as User;
use Siza\Database\App\Models\Spsm\Pengguna;

class SizaUserProvider implements UserProvider
{
    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials): bool
    {
        $password = $credentials['password'];

        if (is_null($password)) {
            return false;
        }

        return Pengguna::where(function($query) use ($password, $user) {
            $query->whereRaw("enpwd(password, 'GET') = '{$password}'")
                ->where(function($query) use ($user) {
                    $query
                        ->where('emp_id', $user->emp_id)
                        ->orWhere('nama_login', $user->emp_id)
                        ->orWhere('nama_login', $user->emp_id.'s');
                });
        })
            ->exists();

    }

    public function retrieveById($identifier)
    {
        return User::query()
            ->where('emp_id', $identifier)
            ->first();
    }

    public function retrieveByToken($identifier, $token)
    {
        // TODO: Implement retrieveByToken() method.
    }

    public function updateRememberToken(UserContract $user, $token)
    {
        // TODO: Implement updateRememberToken() method.
    }

    public function retrieveByCredentials(array $credentials)
    {
        return User::query()
            ->where('emp_id', $credentials['emp_id'])
            ->first();
    }
}