<?php

namespace Siza\Auth\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class SizaInstallCommand extends Command
{
    const STUB_DIR = __DIR__.'/../../stubs';

    public $signature = 'siza-auth:install';

    public $description = 'Install package preset, with views, resources and some other features.';

    public function handle()
    {
        // Modify /config/app.php
        $this->replaceInFile(
            "'timezone' => 'UTC',".PHP_EOL,
            "'timezone' => 'Asia/Kuala_Lumpur',".PHP_EOL,
            base_path('config/app.php')
        );

        $this->replaceInFile(
            "'locale' => 'en',".PHP_EOL,
            "'locale' => 'ms',".PHP_EOL,
            base_path('config/app.php')
        );

        $this->appendToFile(
            "Route::redirect('/', '/admin/login', 301);",
            base_path('routes/web.php')
        );

        // Modify /app/Http/Middleware/VerifyCsrfToken
        $this->replaceInFile(
            '];'.PHP_EOL,
            "\t'/callback/login',".PHP_EOL."\t];".PHP_EOL,
            app_path('Http/Middleware/VerifyCsrfToken.php')
        );

        // Modify /config/app.php
        $this->replaceInFile(
            "'timezone' => 'UTC',".PHP_EOL,
            "'timezone' => 'Asia/Kuala_Lumpur',".PHP_EOL,
            base_path('config/app.php')
        );

        // Modify /.env.example
        $this->replaceInFile(
            "APP_URL=http://localhost".PHP_EOL,
            "APP_URL=http://localhost\n".PHP_EOL."SIZA_AUTH_REMOTE_LOGIN_URL=https://app-siza-my-v2.test".PHP_EOL."SIZA_AUTH_SUB_APP=true".PHP_EOL."SIZA_AUTH_USE_REMOTE_LOGIN=false".PHP_EOL."",
            base_path('.env.example')
        );

        // Modify /.env
        $this->replaceInFile(
            "APP_URL=http://localhost".PHP_EOL,
            "APP_URL=http://localhost\n".PHP_EOL."SIZA_AUTH_REMOTE_LOGIN_URL=https://app-siza-my-v2.test".PHP_EOL."SIZA_AUTH_SUB_APP=true".PHP_EOL."SIZA_AUTH_USE_REMOTE_LOGIN=false".PHP_EOL."",
            base_path('.env')
        );

        File::delete(app_path('Providers/Filament/AdminPanelProvider.php'));
        File::copy(self::STUB_DIR.'/AdminPanelProvider.stub', app_path('Providers/Filament/AdminPanelProvider.php'));

        File::delete(config_path('database.php'));
        File::copy(self::STUB_DIR.'/database.stub', config_path('database.php'));

        File::delete(config_path('auth.php'));
        File::copy(self::STUB_DIR.'/auth.stub', config_path('auth.php'));

        File::delete(base_path('routes/web.php'));
        File::copy(self::STUB_DIR.'/web.stub', base_path('routes/web.php'));

        File::delete(app_path('Models/User.php'));
        File::copy(self::STUB_DIR.'/User.stub', app_path('Models/User.php'));

        if (! File::isDirectory(base_path('lang'))) {
            File::makeDirectory(base_path('lang'));
        }

        if (! File::isDirectory(base_path('lang/vendor'))) {
            File::makeDirectory(base_path('lang/vendor'));
        }

        File::delete(base_path('lang/ms.json'));
        File::copy(self::STUB_DIR.'/ms.json', base_path('lang/ms.json'));

        File::delete(base_path('lang/en.json'));
        File::copy(self::STUB_DIR.'/en.json', base_path('lang/en.json'));

        File::deleteDirectory(base_path('lang/vendor'));
        File::copyDirectory(self::STUB_DIR.'/lang', base_path('lang/vendor'));
    }

    /**
     * Replace a given string within a given file.
     *
     * @param string $search
     * @param string $replace
     * @param string $path
     *
     * @return void
     */
    protected function replaceInFile($search, $replace, $path)
    {
        file_put_contents($path, str_replace($search, $replace, file_get_contents($path)));
    }

    /**
     * Appends a given string to a given file.
     *
     * @param string $append
     * @param string $path
     *
     * @return void
     */
    protected function appendToFile($append, $path)
    {
        $content = file_get_contents($path) . "\n // Added redirection \n" . $append . "\n";
        file_put_contents($path, $content);
    }
}
