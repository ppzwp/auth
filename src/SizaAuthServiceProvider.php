<?php

namespace Siza\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Siza\Auth\Commands\SizaInstallCommand;

class SizaAuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/siza-auth.php', 'siza-auth');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setup();

        $this->registerConsoleCommands();

        $this->registerAuthProvider();
    }

    /**
     * @return void
     */
    protected function setup(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'siza-auth');

        $this->loadRoutesFrom(__DIR__ . '/../routes/siza.php');

        $this->publishes([
            __DIR__ . '/../config/siza-auth.php' => config_path('siza-auth.php')
        ], 'siza-auth-config');
    }

    /**
     * @return void
     */
    public function registerAuthProvider(): void
    {
        Auth::provider('siza-driver', function ($app, array $config) {
            return new SizaUserProvider();
        });
    }

    /**
     * @return void
     */
    public function registerConsoleCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                SizaInstallCommand::class
            ]);
        }
    }
}
