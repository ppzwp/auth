<?php

return [
    /*
     * The main URL that provides the login for the sub-apps.
     */
    'main-url' => env('SIZA_AUTH_REMOTE_LOGIN_URL', 'https://app.siza.my'),

    /*
     * Set to true to use remote login for the app. Basically the login
     * will be handled by https://app.siza.my. If the value is set to
     * false, local authentication will be used.
     */
    'use-remote-login' => env('SIZA_AUTH_USE_REMOTE_LOGIN', true),

    /*
     * Indicates that the current app is a sub-app
     */
    'sub-app' => env('SIZA_AUTH_SUB_APP', true),
];