# Installation

Install the package using composer

```composer require siza/auth```

Finish the Filament installation

```php artisan filament:install --panels```

Run install command

```php artisan siza-auth:install```

## Requirements

- Laravel v10.x
- Filament v3.1.x (automatically installed)