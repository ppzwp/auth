<?php

return [

    'title' => 'Tambah :label',

    'breadcrumb' => 'Tambah',

    'form' => [

        'actions' => [

            'cancel' => [
                'label' => 'Batal',
            ],

            'create' => [
                'label' => 'Tambah',
            ],

            'create_another' => [
                'label' => 'Simpan & tambah baru',
            ],

        ],

    ],

    'notifications' => [

        'created' => [
            'title' => 'Diciptakan',
        ],

    ],

];
