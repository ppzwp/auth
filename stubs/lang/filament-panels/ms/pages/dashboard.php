<?php

return [

    'title' => 'Dashboard',

    'actions' => [

        'filter' => [

            'label' => 'Tapisan',

            'modal' => [

                'heading' => 'Tapisan',

                'actions' => [

                    'apply' => [

                        'label' => 'Mohon',

                    ],

                ],

            ],

        ],

    ],

];
