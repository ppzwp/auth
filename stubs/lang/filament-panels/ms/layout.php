<?php

return [

    'direction' => 'ltr',

    'actions' => [

        'billing' => [
            'label' => 'Urus langganan',
        ],

        'logout' => [
            'label' => 'Log keluar',
        ],

        'open_database_notifications' => [
            'label' => 'Buka pemberitahuan',
        ],

        'open_user_menu' => [
            'label' => 'Menu Pengguna',
        ],

        'sidebar' => [

            'collapse' => [
                'label' => 'Runtuhkan bar sisi',
            ],

            'expand' => [
                'label' => 'Kembangkan bar sisi',
            ],

        ],

        'theme_switcher' => [

            'dark' => [
                'label' => 'Mod gelap',
            ],

            'light' => [
                'label' => 'Mod terang',
            ],

            'system' => [
                'label' => 'Tema sistem',
            ],

        ],

    ],

];
