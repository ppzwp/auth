<?php

return [

    'single' => [

        'label' => 'Tambah',

        'modal' => [

            'heading' => 'Tambah :label',

            'actions' => [

                'create' => [
                    'label' => 'Tambah',
                ],

                'create_another' => [
                    'label' => 'Simpan & tambah baru',
                ],

            ],

        ],

        'notifications' => [

            'created' => [
                'title' => 'Dicipta',
            ],

        ],

    ],

];
