<?php

return [

    'single' => [

        'label' => 'Papar',

        'modal' => [

            'heading' => 'Papar :label',

            'actions' => [

                'close' => [
                    'label' => 'Tutup',
                ],

            ],

        ],

    ],

];
