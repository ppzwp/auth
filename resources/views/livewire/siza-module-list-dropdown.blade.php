<div>
    <x-filament::input.wrapper>
        <x-filament::input.select wire:model="url" wire:change="redirectToModule">
            @foreach ($modules as $module)
                @if (auth()->user()->hasAccessLevel($module->access))
                    <option value="{{ $module->url }}" {{ $module->url == $url ? 'selected' : '' }}>
                        {{ $module->name }}
                    </option>
                @endif
            @endforeach
        </x-filament::input.select>
    </x-filament::input.wrapper>
</div>
