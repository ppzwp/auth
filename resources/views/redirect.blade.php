<form action="{{ $url }}" method="get" id="form-redirect">
    <input type="hidden" name="id" value="{{ $id }}">
    <input type="hidden" name="key" value="{{ $key }}">
</form>
<script>
    document.getElementById('form-redirect').submit();
</script>
