<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

if (config('siza-auth.sub_app') AND config('siza-auth.use_remote_login')) {
    Route::get('/admin/login', function () {
        return redirect(config('siza-auth.main-url').'/admin');
    })->middleware('guest')->name('login');

    Route::post('/admin/login', function (Request $request) {
        // Force logout previous session
        Auth::guard('web')->logout();

        Auth::guard('web')->loginUsingId($request->id);

        return redirect()->route('dashboard');
    })->middleware('web');
}
else {
    Route::redirect('/login', config('siza-auth.main-url').'/admin/login');
}

Route::post('/callback/login', function (Request $request) {
    // Force logout previous session
    Auth::guard('web')->logout();

    Auth::guard('web')->loginUsingId($request->id);

    return redirect()->route('dashboard');
})->middleware('web');